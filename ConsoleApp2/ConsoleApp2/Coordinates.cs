﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public struct Coordinates
    {
        private int x { get; set; }
        private int y { get; set; }
        public Coordinates(int x_, int y_)
        {
            x = x_;
            y = y_;
        }
    }
}
