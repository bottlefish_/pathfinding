﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class midpointcircle
    {
        //List<Coordinates> circlePoints = midpointcircle.PlotCircle2(30, 30, 20);
        //
        //
        //    for (int y = 0; y< 50; y++)
        //    {
        //        for (int x = 0; x< 50; x++)
        //        {
        //            Coordinates currentPoint = new Coordinates(x, y);
        //            if (circlePoints.Contains(currentPoint))
        //            {
        //                Console.Write("#");
        //            }
        //            else
        //            {
        //                Console.Write("*");
        //            }
        //        }
        //        Console.WriteLine();
        //    }
        //    Console.ReadLine();
        


        private static void Plot8points(List<Coordinates> _circlePoints, int x, int y, int xOrigin, int yOrigin)
        {
            
            _circlePoints.Add(new Coordinates(xOrigin+ x, yOrigin + y));
            _circlePoints.Add(new Coordinates(xOrigin+ y, yOrigin + x));
            _circlePoints.Add(new Coordinates(xOrigin- y, yOrigin + x));
            _circlePoints.Add(new Coordinates(xOrigin- x, yOrigin + y));
            _circlePoints.Add(new Coordinates(xOrigin- x, yOrigin - y));
            _circlePoints.Add(new Coordinates(xOrigin- y, yOrigin - x));
            _circlePoints.Add(new Coordinates(xOrigin+ y, yOrigin - x));
            _circlePoints.Add(new Coordinates(xOrigin+ x, yOrigin - y));
        }

        public static List<Coordinates> PlotCircle2(int x0, int y0, int radius)
        {
            List<Coordinates> circlePoints = new List<Coordinates>();
            int x = 0;
            int y = radius;
            int h = 1 - radius;
            int deltaE = 3;
            int deltaSE = -2 * radius + 5;
            Plot8points(circlePoints, x, y, x0, y0);
            while (y > x)
            {
                if (h < 0) /* select E */
                {
                    h += deltaE;
                    deltaE += 2;
                    deltaSE += + 2;
                }
                else /* select SE */
                {
                    h += deltaSE;
                    deltaE += 2;
                    deltaSE += 4;
                    y = y - 1;
                }
                x = x + 1;
                Plot8points(circlePoints, x, y, x0, y0);
            }
            return circlePoints;

        }
        private static List<Coordinates> PlotCircle(int x0, int y0, int radius )
        {
            int x = radius;
            int y = 0;
            int dx = 1;
            int dy = 1;
            int err = 0;
            List<Coordinates> circlePoints = new List<Coordinates>();

            while (x >= y)
            {
                circlePoints.Add(new Coordinates(x0 + x, y0 + y));
                circlePoints.Add(new Coordinates(x0 + y, y0 + x));
                circlePoints.Add(new Coordinates(x0 - y, y0 + x));
                circlePoints.Add(new Coordinates(x0 - x, y0 + y));
                circlePoints.Add(new Coordinates(x0 - x, y0 - y));
                circlePoints.Add(new Coordinates(x0 - y, y0 - x));
                circlePoints.Add(new Coordinates(x0 + y, y0 - x));
                circlePoints.Add(new Coordinates(x0 + x, y0 - y));

                if (err <= 0)
                {
                    y++;
                    err += dy;
                    dy += 2;
                }

                if (err > 0)
                {
                    x--;
                    dx += 2;
                    err += dx - (2*radius);
                }
            }
            return circlePoints;
        }
    }
}
